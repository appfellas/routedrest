package routed_rest

import org.scalatest.FunSpec

/**
 * Created by mb on 16-04-15.
 */
class RESTAsyncPlanSpec extends FunSpec {

  describe("A Set") {
    describe("when empty") {
      it("should have size 0") {
        assert(Set.empty.size == 0)
      }

      it("should produce NoSuchElementException when head is invoked") {
        intercept[NoSuchElementException] {
          Set.empty.head
        }
      }
    }
  }
}
