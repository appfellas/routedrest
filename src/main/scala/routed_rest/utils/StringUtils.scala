package routed_rest.utils

import scala.util.Random

/**
 * Created by mb on 18-04-15.
 */
object StringUtils {

  def actorName(name: String, symbols: Int = 4): String = {
    val x = Random.alphanumeric
    name + "-" + (x take symbols mkString)
  }

}
