package routed_rest.config

import routed_rest.controllers.CustomersController
import routed_rest.{Logging, RoutedRest, RestRouter}

/**
 *
 *
 * User: mb
 * Date: 31-03-14
 * Time: 14:04
 * Package: routed_rest
 *
 * Copyright AppFellas B.V. 2012-2014
 */
class Routes extends RestRouter with Logging {
  RoutedRest.configure( router => {

    router filteredParams List("password", "access_token", "session_id")

    router setURLRoot "/api"

    // path |  controller class | method/action name
    router get("/customers", classOf[CustomersController], "all")

    router get("/customers/test", classOf[CustomersController], "test")

    router post("/customers", classOf[CustomersController], "post")
    router put("/customers", classOf[CustomersController], "put")
    router delete("/customers", classOf[CustomersController], "delete")
    router options("/customers", classOf[CustomersController], "options")
    router head("/customers", classOf[CustomersController], "head")

    router get("/customer/:id", classOf[CustomersController], "customer")
    router get("/customer/:id/:artist_id", classOf[CustomersController], "customer")


  })

}
