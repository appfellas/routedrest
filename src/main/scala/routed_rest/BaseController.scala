package routed_rest

import _root_.akka.actor.{ActorRef, ActorSystem}

import scala.collection.mutable
import scala.language.dynamics

/**
 *
 *
 * User: mb
 * Date: 30-03-14
 * Time: 10:50
 * Package: routed_rest
 *
 * Copyright AppFellas B.V. 2012-2014
 */
class BaseController
  extends Dynamic
  with Logging {

  private val beforeFilters = mutable.Queue[String]()
  private val afterFilters = mutable.Queue[String]()

  var _params: Map[String, String] = _

  var _headers: Map[String, String] = _

  var _system: ActorSystem = _

  var _actor: ActorRef = _

  implicit var _requestId: String = _

  def params(key: String): Option[String] = {
    if (_params.nonEmpty && _params.keys.exists(k => k == key)) {
      Option(_params(key))
    } else {
      log.debug("Didn't find key: "+key)
      None
    }
  }

  def params(): Map[String, String] = {
    _params
  }

  def headers(key: String): Option[String] = {
    if (_headers.nonEmpty && _headers.keys.exists(k => k == key)) {
      Option(_headers(key))
    } else {
      None
    }
  }

  def headers(): Map[String, String] = {
    _headers
  }




  def invokeMethod(methodName: String) = {

    try {
      val clazz = getClass
      val method = clazz.getMethod(methodName)

      method.invoke(this)
    } catch {
      case e: scala.ScalaReflectionException =>
        log.debug("Error calling filter: " + e.getMessage)
      //req.respond(ResponseString("Unknown action " + typeOf[T].toString + "#" + action) ~> NotFound)
      case e: java.lang.NoSuchMethodException =>
        log.debug("Error calling filter: " + e.getMessage)
      //req.respond(ResponseString("Unknown action " + typeOf[T].toString + "#" + action) ~> NotFound)
    }

  }

  def beforeFilter(filterName: String) = {
    //TODO Call filterName on current class before calling request
    if (!beforeFilters.contains(filterName)) {
      beforeFilters.enqueue(filterName)
    }
    //invokeMethod(filterName)
  }

  def afterFilter(filterName: String) = {
    //TODO Call filterName on current class after request is performed
    if (!afterFilters.contains(filterName)) {
      afterFilters.enqueue(filterName)
    }
    //invokeMethod(filterName)
  }


//  def applyDynamic(methodName: String)(args: Any*) {
//    log.debug("Method is not found: " + methodName)
//  }


}
