package routed_rest

import java.net.URL
import javax.servlet.http.{HttpServletRequest, HttpServletResponse}

import _root_.akka.actor.{ActorRef, ActorSystem, Props}
import routed_rest.Messages.{CatchAllMessage, DoRequestMessage}
import routed_rest.akka.RoutedActor
import routed_rest.akka.RoutedActorMessages.RoutedRestRequest
import routed_rest.utils.StringUtils
import unfiltered.Async.Responder
import unfiltered.request.HttpRequest

import scala.reflect.ClassTag

import scala.reflect.runtime.universe._



object Messages {


  case class DoRequestMessage(method: String,
                              controllerClass: Class[_],
                              action: String,
                              tag: TypeTag[_],
                              classTag: ClassTag[_],
                              req: HttpRequest[HttpServletRequest] with Responder[HttpServletResponse],
                              requestId: Long)

  case class CatchAllMessage(req: HttpRequest[HttpServletRequest] with Responder[HttpServletResponse], requestId: Long)

}

/**
 *
 *
 * User: mb
 * Date: 31-03-14
 * Time: 14:25
 * Package: routed_rest
 *
 * Copyright AppFellas B.V. 2012-2014
 */


object RESTAsyncPlan extends unfiltered.filter.async.Plan with Routing {

  var routes: (RESTAsyncPlan.type) => Unit = _
  var _API_ROOT: String = ""

  var _system: ActorSystem = _

  var rootActor: ActorRef = _

  def system(system: ActorSystem) = {
    _system = system
    rootActor = _system.actorOf(Props[RoutedActor].withDispatcher("route-dispatcher"), StringUtils.actorName("routedA"))
    RESTAsyncPlan
  }

  def intent = {
    case r =>

      rootActor ! RoutedRestRequest(r, System.nanoTime)

  }

  def initRoutes(rts: (RESTAsyncPlan.type) => Unit) = {
    RESTAsyncPlan.routes = rts
    RESTAsyncPlan.routes(this)
  }

  def setURLRoot(root: String) = {
    RESTAsyncPlan._API_ROOT = root.stripPrefix("/")
  }
}

class RESTAsyncPlan(val req: HttpRequest[HttpServletRequest] with Responder[HttpServletResponse],
                    system: ActorSystem,
                     requestId: Long) extends Logging {

  //log.debug(s"[$requestId] Started ${req.method} ${req.uri} ReqId[ $requestId ] ")

  var firstMatchFound = false

  def stripParams(url: String): String = {
    val requestedUri = new URL("http://s.com" + url) //Note: s.com just to have a correct URL
    requestedUri.getPath
  }

  def intent() = {

    //Processing requests from REST here

    //log.info(s"[$requestId] Started ${req.method} ${req.uri} from ${req.remoteAddr}")

    var matches = false
    var path = stripParams(req.uri)

    object AllDone extends Exception {}

    //Resolve URL to a pattern if any
    try {
      log.debug(s"[$requestId] Got routeMap of " + RESTAsyncPlan.routeMap.size + " routes")

      implicit val _requestId = requestId

      RESTAsyncPlan.routeMap.foreach {
        route =>
          if (!firstMatchFound) {

            if (RESTAsyncPlan.matchPattern(stripParams(req.uri), route._1._1)) {
              matches = true
              path = route._1._1
              firstMatchFound = true
              log.debug(s"[$requestId] First Match Found")
              throw AllDone
            }
          } else {
            log.debug(s"[$requestId] Not first match: " + req.uri)
          }
      }
    } catch {

      case AllDone =>
        log.debug(s"[$requestId] All Done")
    }


    if (RESTAsyncPlan.routeMap.contains(key = (path, req.method))) {
      implicit val request = req
      val entry = RESTAsyncPlan.routeMap((path, req.method))
      //log.debug(s"[$requestId] Do request: " + Option(request.uri))
      val msg = DoRequestMessage(entry.method, entry.controller, entry.action, entry.typeTag, entry.classTag, req, requestId)
      system.actorOf(Props[RoutedActor].withDispatcher("request-dispatcher"), StringUtils.actorName("restReqActor")) ! msg

    } else {
      //log.debug(s"[$requestId] Catch all unmapped routes")
      system.actorOf(Props[RoutedActor].withDispatcher("request-dispatcher"), StringUtils.actorName("catchAllActor")) ! CatchAllMessage(req, requestId)

    }


  }
}