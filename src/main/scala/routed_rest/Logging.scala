package routed_rest

import org.slf4j.LoggerFactory

/**
 *
 *
 * User: mb
 * Date: 31-03-14
 * Time: 15:26
 * Package: routed_rest
 *
 * Copyright AppFellas B.V. 2012-2014
 */
trait Logging {
  val log = LoggerFactory.getLogger(this.getClass.getName)
}
