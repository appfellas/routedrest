package routed_rest

import _root_.akka.actor.ActorSystem
import unfiltered.request.HttpRequest
import javax.servlet.http.{HttpServletResponse, HttpServletRequest}


/**
 *
 *
 * User: mb
 * Date: 31-03-14
 * Time: 14:26
 * Package: routed_rest
 *
 * Copyright AppFellas B.V. 2012-2014
 */

object RoutedRest {

  def configure(f: (RESTAsyncPlan.type) => Unit) = {
    RESTAsyncPlan.initRoutes(f)
  }

}

class RoutedRest[T <: RestRouter](_routes: T, system: ActorSystem) {

  def asyncPlan: RESTAsyncPlan.type = {
    RESTAsyncPlan.system(system)
  }




}
