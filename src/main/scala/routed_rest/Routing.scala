package routed_rest

import java.lang.reflect.InvocationTargetException
import java.net.URI
import javax.servlet.http.HttpServletResponse

import _root_.akka.actor.{ActorRef, ActorSystem}
import routed_rest.akka.{ResponseMsg, ErrorResponseMsg}

import scala.collection.immutable.TreeMap
import scala.reflect.ClassTag
import scala.reflect.runtime.universe._


case class ControllerResponse(res: unfiltered.response.ResponseFunction[HttpServletResponse])

/**
 *
 *
 * User: mb
 * Date: 29-03-14
 * Time: 20:52
 * Package:
 *
 * Copyright AppFellas B.V. 2012-2014
 */

trait AbstractRouteEntry {

}

case class RouteEntry(pattern: String, method: String, controller: Class[_], action: String, typeTag: TypeTag[_], classTag: ClassTag[_]) extends AbstractRouteEntry

trait Routing extends Logging {

  import javax.servlet.http.{HttpServletRequest, HttpServletResponse}

  import unfiltered.Async.Responder
  import unfiltered.request.HttpRequest
  import unfiltered.response._

  var _requestHandled = false
  val ru = scala.reflect.runtime.universe

  var routeMap: TreeMap[(String, String), RouteEntry] = TreeMap[(String, String), RouteEntry]()

  var extractedParams = Map[String, String]()

  var filteredParams: List[String] = List()

  def capitalize(s: String) = {
    s(0).toUpper + s.substring(1, s.length).toLowerCase
  }

  /**
   * Invoke a method on a given class
   *
   * @param action    Method to invoke
   * @param req       Rest Request
   * @param params    HTTP Params from client
   * @param headers   HTTP Headers
   * @param system    AKKA System Reference
   * @param actor     AKKA Actor Reference
   * @param tag
   * @param clTag
   * @tparam T
   * @return
   */
  def invokeAction[T](action: String,
                      req: HttpRequest[HttpServletRequest] with Responder[HttpServletResponse],
                      params: Map[String, String],
                      headers: Map[String, String],
                      system: ActorSystem,
                      actor: ActorRef,
                      requestId: Long)(implicit tag: TypeTag[T], clTag: ClassTag[T]) = {

    val classX = Class.forName(ru.typeOf[T].typeSymbol.fullName)
    val args = Array[AnyRef](req)
    val constructor = classX.getConstructors()(0)
    val controllerObj = constructor.newInstance(args: _*) //.asInstanceOf[T]

    //Inject params
    classX.getMethods.find(_.getName == "_params_$eq").get.invoke(controllerObj, params)
    //Inject headers
    classX.getMethods.find(_.getName == "_headers_$eq").get.invoke(controllerObj, headers)
    //Inject Akka System reference
    classX.getMethods.find(_.getName == "_system_$eq").get.invoke(controllerObj, system)
    //Inject Parent Actor reference
    classX.getMethods.find(_.getName == "_actor_$eq").get.invoke(controllerObj, actor)
    //Inject Request ID
    classX.getMethods.find(_.getName == "_requestId_$eq").get.invoke(controllerObj, requestId.toString)

    //TODO Intercept possible exceptions from the controller
    //TODO Simple try/catch doesn't catch the message from the exception
    classX.getMethod(action).invoke(controllerObj)
  }

  /**
   * Matches route pattern
   *
   * @param requested Requested pattern
   * @param defined   Defined pattern
   * @return
   */
  def matchPattern(requested: String, defined: String)(implicit requestId: Long): Boolean = {

    val requestedArr = requested.split("/").filter(el => !el.isEmpty)
    log.debug(s"[$requestId] Requested route: " + requestedArr.mkString(" | "))
    val definedArr = (defined.split("/").reverse :+ RESTAsyncPlan._API_ROOT).reverse.filter(el => !el.isEmpty)
    log.debug(s"[$requestId]   Defined route: " + definedArr.mkString(" | "))
    var count = 0

    if (requestedArr.length != definedArr.length) {
      return false
    }

    var params = Map[String, String]()


    definedArr.foreach { part =>

      if (part.contains(":")) {
        //log.debug("Part is a pattern: " + part.stripPrefix(":"))
        //TODO Clean input
        //
        params += (part.stripPrefix(":").trim -> requestedArr(count).trim)

      } else {
        if (part != requestedArr(count)) {
          return false
        }
      }

      count += 1
    }

    log.debug(s"[$requestId] Extracted params: " + filterParams(params))
    extractedParams = params

    true
  }

  def doRequest(method: String,
                controllerClass: Class[_],
                action: String,
                tag: TypeTag[_],
                classTag: ClassTag[_],
                system: ActorSystem,
                actor: ActorRef,
                requestId: Long)(implicit req: HttpRequest[HttpServletRequest] with Responder[HttpServletResponse]) = {

    implicit val typeTag = tag
    implicit val clTag = classTag

    var params = (for {
      key <- req.parameterNames
      value <- req.parameterValues(key)
    } yield {
        key -> value
      }).toMap

    val headers = (for {
      key <- req.headerNames
      value <- req.headers(key)
    } yield {
        key -> value
      }).toMap

    params = params ++ extractedParams

    //log.debug(s"[$requestId] Params in Do Request: " + filterParams(params))
    log.info(s"[$requestId] Processing " + method + " " + new URI(req.uri).getPath + " by " + controllerClass.getSimpleName + "#" + action +
      " \n" + "Params: {" + filterParams(params).mkString(" , ") + "}")
    //log.debug(s"[$requestId] Processing " + method + " " + req.uri + " \n" + "Params: {" + filterParams(params).mkString(" , ") + "}")

    try {
      invokeAction(action, req, params, headers, system, actor, requestId)
    } catch {
      case e: scala.ScalaReflectionException =>
        log.error(s"[$requestId] Routing error: " + e.getMessage)
        actor ! ResponseMsg(NotFound.code, "Unknown method " + controllerClass.getName + "#" + action) //ControllerResponse(ResponseString("Unknown action " + controllerClass.getName + "#" + action) ~> NotFound)

      case e: java.lang.NoSuchMethodException =>
        log.error(s"[$requestId] Routing error: " + e.getMessage)
        actor ! ResponseMsg(NotFound.code, "Unknown method " + controllerClass.getName + "#" + action) //ControllerResponse(ResponseString("Unknown action " + controllerClass.getName + "#" + action) ~> NotFound)

      case e: InvocationTargetException =>
        val target = e.getTargetException
        log.error(s"[$requestId] Caught exception from " + controllerClass.getSimpleName + "#" + action + ": " + target.getMessage)
        log.error(s"[$requestId] " + target.getStackTrace.mkString("\n"))

        val msg = ErrorResponseMsg(InternalServerError.code, "Internal Server Error", target.getStackTrace.mkString("\n"),
          "Caught exception from " + controllerClass.getSimpleName +
          "#" + action + ": " + target.getMessage)

        actor ! msg

      case e: Exception =>
        log.error(s"[$requestId] Caught exception from " + controllerClass.getSimpleName + "#" + action + ": " + e.getLocalizedMessage)
        log.error(s"[$requestId] " + e.getStackTrace.mkString("\n"))

        actor ! ErrorResponseMsg(InternalServerError.code, "Internal Server Error", e.getStackTrace.mkString("\n"),
          "Caught exception from " + controllerClass.getSimpleName +
            "#" + action + ": " + e.getMessage)
    }


  }

  def filterParams(params: Map[String, String], replacement: String = "[filtered]"): Map[String, String] = {
    params.map {
      case (k, v) =>
        if (filteredParams.contains(k)) {
          k -> replacement
        } else {
          k -> v
        }
    }
  }

  def get[T](path: String, controllerClass: Class[T], action: String)(implicit tag: TypeTag[T],
                                                                      clTag: ClassTag[T]
    ) = {

    val entry = RouteEntry(path, "GET", controllerClass, action, tag, clTag)
    routeMap = routeMap + ((path, "GET") -> entry)
    log.debug("RouteMap in Get: " + routeMap.size)
  }

  def post[T](path: String, controllerClass: Class[T], action: String)(implicit tag: TypeTag[T],
                                                                       clTag: ClassTag[T]) = {

    val entry = RouteEntry(path, "POST", controllerClass, action, tag, clTag)
    routeMap = routeMap + ((path, "POST") -> entry)
  }

  def put[T](path: String, controllerClass: Class[T], action: String)(implicit tag: TypeTag[T],
                                                                      clTag: ClassTag[T]) = {
    val entry = RouteEntry(path, "PUT", controllerClass, action, tag, clTag)
    routeMap = routeMap + ((path, "PUT") -> entry)
  }

  def delete[T](path: String, controllerClass: Class[T], action: String)(implicit tag: TypeTag[T],
                                                                         clTag: ClassTag[T]) = {

    val entry = RouteEntry(path, "DELETE", controllerClass, action, tag, clTag)
    routeMap = routeMap + ((path, "DELETE") -> entry)
  }

  def options[T](path: String, controllerClass: Class[T], action: String)(implicit tag: TypeTag[T],
                                                                          clTag: ClassTag[T]): Unit = {

    val entry = RouteEntry(path, "OPTIONS", controllerClass, action, tag, clTag)
    routeMap = routeMap + ((path, "OPTIONS") -> entry)

  }

  def head[T](path: String, controllerClass: Class[T], action: String)(implicit tag: TypeTag[T],
                                                                       clTag: ClassTag[T]): Unit = {

    val entry = RouteEntry(path, "HEAD", controllerClass, action, tag, clTag)
    routeMap = routeMap + ((path, "HEAD") -> entry)

  }

  def filteredParams(params: List[String]): Unit = {
    filteredParams = params
  }

  /**
   * Catch all unknown routes
   * @param req
   */
  def catchAll(req: HttpRequest[HttpServletRequest] with Responder[HttpServletResponse], actor: ActorRef, requestId: Long) = {
    if (!_requestHandled) {
      //TODO Format routes
      log.debug(s"[$requestId] Route not found: " + req.uri+ "\nAvailable Routes:\n"+routeMap.mkString("\n"))
      actor ! ResponseMsg(NotFound.code, "Route not found: " + req.uri) //ControllerResponse(ResponseString("Unknown action " + controllerClass.getName + "#" + action) ~> NotFound)
       //ControllerResponse(ResponseString(s"[$requestId] Unknown Route: " + req.uri) ~> BadRequest)
      _requestHandled = false
    } else {
      log.debug(s"[$requestId] Request already handler: " + req.uri)
      actor ! ResponseMsg(NotFound.code, "Request was already handled:" + req.uri) //ControllerResponse(ResponseString("Request was already handled: " + req.uri) ~> BadRequest)
    }

  }

}
