package routed_rest.controllers

import java.util.Date
import javax.servlet.http.{HttpServletRequest, HttpServletResponse}

import routed_rest.BaseController
import routed_rest.akka.{ResponseMsg, RootObjectResponseMsg}
import unfiltered.Async.Responder
import unfiltered.request.HttpRequest
import unfiltered.response.Ok

/**
 * Example controller
 *
 *
 * User: mb
 * Date: 30-03-14
 * Time: 10:50
 * Package: routed_rest.controllers
 *
 * Copyright AppFellas B.V. 2012-2014
 */

case class ExampleObjectResponse(id: Int, name: String)

case class ResponseWithDate(id: Int, date: Date)

class CustomersController(request: HttpRequest[HttpServletRequest] with Responder[HttpServletResponse]) extends BaseController  {

  beforeFilter("beforeFilter")
  afterFilter("afterFilter")


  def beforeFilter() = {
    log.debug("Called before filter")
  }

  def afterFilter() = {
    log.debug("Called after filter")
  }

  def all() = {

    log.debug(s"[${_requestId}] All called: "+request.uri)
    log.debug(s"[${_requestId}] For params: "+params().mkString(", "))
    log.debug(s"[${_requestId}] Remote IP: "+request.remoteAddr)
    log.debug(s"[${_requestId}] Headers: "+headers().mkString(" | "))
    //_system - ActorSystem
    //_actor - Caller Actor

    _actor ! RootObjectResponseMsg(Ok.code, "studies",
      List(ExampleObjectResponse(10, "SomeName"),
        ExampleObjectResponse(10, "SomeName"),
        ResponseWithDate(10,  new Date())))
  }


  def post() = {
    _actor ! ResponseMsg(Ok.code, "Got POST")
  }

  def put() = {
    _actor ! ResponseMsg(Ok.code, "Got PUT")
  }

  def delete() = {
    _actor ! ResponseMsg(Ok.code, "Got DELETE")
  }

  def options() = {
    _actor ! ResponseMsg(Ok.code, "Got OPTIONS")
  }

  //TODO This has to respond in some special way
  def head() = {
    _actor ! ResponseMsg(Ok.code, "Got HEAD")
  }


  def customer() = {

    log.debug(s"[${_requestId}] Customer details called: "+request.uri)
    log.debug(s"[${_requestId}] For params: "+params().mkString(", "))

    throw new Exception("Throwing an exception from here")




    _actor ! ResponseMsg(Ok.code, "Customer details called for id: "+params("id") +" "+params("artist_id"))
  }


}


