package routed_rest.akka

import javax.servlet.http.{HttpServletRequest, HttpServletResponse}

import akka.actor.{Actor, ActorLogging}
import routed_rest.Messages.{CatchAllMessage, DoRequestMessage}
import routed_rest.akka.RoutedActorMessages.RoutedRestRequest
import routed_rest.{ControllerResponse, RESTAsyncPlan}
import unfiltered.Async.Responder
import unfiltered.request.HttpRequest
import unfiltered.response.{ResponseHeader, Status}


object RoutedActorMessages {

  case class RoutedRestRequest(req: HttpRequest[HttpServletRequest] with Responder[HttpServletResponse], requestId: Long)


}

abstract class RestResponseMsg {
  def status: Int
}

case class ResponseMsg(status: Int, message: String) extends RestResponseMsg

case class ErrorResponseMsg( val status: Int,  val message: String, stacktrace: String, devMessage: String)

case class ObjectResponseMsg(status: Int, obj: Any) extends RestResponseMsg

case class RootObjectResponseMsg(status: Int, objName: String, obj: Any) extends RestResponseMsg


class RoutedActor extends Actor with ActorLogging {

  var _request: HttpRequest[HttpServletRequest] with Responder[HttpServletResponse] = _
  var _requestId: Long = _

  import org.json4s.JsonDSL._
  import org.json4s._
  import org.json4s.jackson.JsonMethods._
  import org.json4s.native.Serialization


  implicit val formats = DefaultFormats
  implicit val formats2 = Serialization.formats(NoTypeHints)

  override def receive: Receive = {

    case r: RoutedRestRequest =>
      log.info(s"[${r.requestId}] Got new REST request: " + r.requestId)

      _request = r.req
      _requestId = r.requestId

      new RESTAsyncPlan(req = r.req, context.system, r.requestId).intent()


    case entry: DoRequestMessage =>
      //log.info(s"[${entry.requestId}] Do Request")
      implicit val req = entry.req
      _request = req
      _requestId = entry.requestId
      RESTAsyncPlan.doRequest(entry.method, entry.controllerClass, entry.action,
        entry.tag, entry.classTag, context.system, context.self, _requestId)

    case req: CatchAllMessage =>
      _request = req.req
      _requestId = req.requestId
      log.info(s"[${_requestId}] Catch all unmapped routes")
      RESTAsyncPlan.catchAll(req.req, context.self, _requestId)


    // Process responses
    case response: ControllerResponse =>
      val diff = (System.nanoTime() - _requestId) / 1000000
      log.info(s"[${_requestId}] Finished in $diff ms")
      //if (DEBUG) {
      val out = ResponseHeader("X-Request-Id", Set(_requestId.toString)) ~> ResponseHeader("X-Request-Duration", Set(diff.toString+ " ms")) ~> response.res
      //}

      _request.respond(out)

    case response: RootObjectResponseMsg =>
      val jsonObj = Extraction.decompose(response.obj)
      val json: JValue = render(response.objName -> jsonObj)
      self ! ControllerResponse(unfiltered.response.Json(json) ~> Status(response.status))

    case response: ObjectResponseMsg =>
      val jsonObj = Extraction.decompose(response.obj)
      self ! ControllerResponse(unfiltered.response.Json(jsonObj) ~> Status(response.status))

    case response: RestResponseMsg =>

      val jsonObj = Extraction.decompose(response)
      val out = unfiltered.response.Json(jsonObj) ~> Status(response.status)
      self ! ControllerResponse(out)

    case response: ErrorResponseMsg =>
      val jsonObj = Extraction.decompose(response)
      val out = unfiltered.response.Json(jsonObj) ~> Status(response.status)
      self ! ControllerResponse(out)
  }

}
