package routed_rest

import _root_.akka.actor.{ActorSystem, Props}
import _root_.akka.util.Timeout
import routed_rest.akka.RoutedActor
import routed_rest.config.Routes
import routed_rest.utils.StringUtils

import scala.concurrent.duration._
import scala.language.postfixOps


object Boot extends App with Logging {

  val system = ActorSystem("routed")
  var routedActor = system.actorOf(Props[RoutedActor], StringUtils.actorName("routed-act"))

  system.registerOnTermination(new Runnable {
    override def run(): Unit = {
      log.debug("System is going to shut down.")
    }
  })

  implicit val timeout = Timeout(1 second)

  val routedRest = new RoutedRest(new Routes, system)

  val PORT_NUMBER = 8080

  unfiltered.jetty.Server.local(PORT_NUMBER).plan(routedRest.asyncPlan).run(
    { s =>
      log.info(s"Unfiltered Server started in port $s.port")
      Runtime.getRuntime.addShutdownHook(ShutdownHook)
    }, { s =>
      log.info("Unfiltered Server stopped")
      system.shutdown()
    })


}

object ShutdownHook extends Thread {
  override def run() = {
    setName("ExampleServiceShutdownHook")
    println("ShutdownHook Invoked")
  }
}
