import sbtrelease.Version
import spray.revolver.RevolverPlugin.Revolver

name := "RoutedRest"

//version := "0.0.1"

scalaVersion := "2.11.6"

Revolver.settings

releaseSettings

resolvers += "sonatype-releases" at "https://oss.sonatype.org/content/repositories/releases/"

resolvers += "Typesafe Simple Repository" at "http://repo.typesafe.com/typesafe/simple/maven-releases/"

resolvers += "Typesafe Releases" at "https://repo.typesafe.com/typesafe/releases/"

//Dependencies

val unfilteredV = "0.8.4"

val akkaV = "2.3.9"

libraryDependencies += "net.databinder" %% "unfiltered" % unfilteredV

libraryDependencies += "net.databinder" %% "unfiltered-jetty" % unfilteredV

//libraryDependencies += "net.databinder" %% "unfiltered-filter" % unfilteredV

//libraryDependencies += "net.databinder" %% "unfiltered-netty" % "0.7.1"

libraryDependencies += "net.databinder" %% "unfiltered-filter-async" % unfilteredV

libraryDependencies += "net.databinder" %% "unfiltered-uploads" % unfilteredV

libraryDependencies += "net.databinder" %% "unfiltered-json4s" % unfilteredV

//libraryDependencies += "net.databinder" %% "unfiltered-oauth2" % unfilteredV

//libraryDependencies += "net.databinder" %% "unfiltered-mac" % unfilteredV

//libraryDependencies += "net.databinder" %% "unfiltered-scalate" % unfilteredV

libraryDependencies += "net.databinder" %% "unfiltered-scalatest" % unfilteredV % "test"

libraryDependencies += "org.json4s" %% "json4s-jackson" % "3.2.11"

// Akka
libraryDependencies += "com.typesafe.akka" %% "akka-actor"    % akkaV

libraryDependencies += "com.typesafe.akka" %% "akka-slf4j"    % akkaV

//libraryDependencies += "com.typesafe.akka" %% "akka-remote"   % akkaV

//libraryDependencies += "com.typesafe.akka" %% "akka-agent"    % akkaV

libraryDependencies += "com.typesafe.akka" %% "akka-testkit"  % akkaV % "test"



//Logging

libraryDependencies += "org.slf4j" % "slf4j-api" % "1.6.4"

libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.0.1"

//libraryDependencies += "ch.qos.logback" % "logback-core" % "1.0.1"

libraryDependencies += "org.scalatest" %% "scalatest" % "2.2.4" % "test"



scalacOptions in ThisBuild ++= Seq("-unchecked", "-deprecation", "-language:dynamics", "-feature", "-language:postfixOps")

scalacOptions ++= Seq("-unchecked", "-deprecation")

publishTo := Some(Resolver.file("file",  new File( "maven-repo"))) //Some("Sonatype Snapshots Nexus" at "https://oss.sonatype.org/content/repositories/snapshots")

//ideaExcludeFolders += ".idea"

//ideaExcludeFolders += ".idea_modules"

//ideaExcludeFolders += "maven-repo"

ReleaseKeys.versionBump := Version.Bump.Next

buildInfoSettings

sourceGenerators in Compile <+= buildInfo

buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion)

buildInfoPackage := "routed_rest"















